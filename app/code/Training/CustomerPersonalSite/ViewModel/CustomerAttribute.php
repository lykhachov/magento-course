<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-08
 * Time: 20:16
 */

namespace Training\CustomerPersonalSite\ViewModel;

class CustomerAttribute implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    public function test()
    {
        return 'test';
    }

    public function getPersonalSite($customerData)
    {
        $attribute = $customerData->getCustomAttribute('personal_site');
        if ($attribute) {
            return $attribute->getValue();
        }
        return '';
    }
}