<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-12
 * Time: 09:14
 */

namespace Training\PluginRouterList\Plugin\Controller;

use Magento\Framework\App\FrontControllerInterface;
use Magento\Framework\App\RouterListInterface;
use Psr\Log\LoggerInterface;

class RouterListToLog
{
    private $routerList;
    private $logger;

    public function __construct(
        RouterListInterface $routerList,
        LoggerInterface $logger
    ) {
        $this->routerList = $routerList;
        $this->logger = $logger;
    }

    public function beforeDispatch(
        FrontControllerInterface $subject
    ) {
        foreach ($this->routerList as $router) :
            $this->logger->info(get_class($router));
        endforeach;
    }
}
