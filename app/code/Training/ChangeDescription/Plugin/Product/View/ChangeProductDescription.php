<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-19
 * Time: 15:21
 */

namespace Training\ChangeDescription\Plugin\Product\View;

class ChangeProductDescription
{
    public function beforeToHtml(
        \Magento\Catalog\Block\Product\View\Description $subject
    ) {
        $subject->getProduct()->setDescription('test description change');
        if ($subject->getNameInLayout() == 'product.info.sku') {
            $subject->setTemplate('Training_ChangeDescription::description.phtml');
        }
    }
}
