<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-05
 * Time: 20:41
 */

namespace Training\AdditionalTestOM\Model;

class AnotherTest
{
    private $testObject;
    private $testObjectFactory;
    private $manager;

    public function __construct(
        \Training\TestOM\Model\Test $testObject,
            \Training\TestOM\Model\TestFactory $testObjectFactory,
        \Training\AdditionalTestOM\Model\ManagerCustomImplementation $manager
    ) {
        $this->testObject = $testObject;
        $this->testObjectFactory = $testObjectFactory;
        $this->manager = $manager;
    }

    public function run()
    {
        echo '<br><br><strong>Test object with custom arguments managed by di.xml</strong><br><br>';
        $this->testObject->log();
        echo '<br><br><strong>Test object with custom constructor arguments</strong><br><br>';
        $customArrayList = ['item1' => 'aaaaaaa','item2' => 'bbbbbb'];
        print_r(get_class($this->testObjectFactory));
        $newTestObject = $this->testObjectFactory->create([
            'arrayList' => $customArrayList,
            'manager'   => $this->manager
        ]);
        $newTestObject->log();
    }
}