define([
    'uiComponent',
    'jquery',
    'ko'
], function (Component, $, ko) {
    'use strict';
    return Component.extend({
        productCount: ko.observable(),
        isLoading: ko.observable(false),
        isLoaded: ko.observable(false),
        url: '',
        initialize: function () {
            this._super();
            console.log(this);
            return this;
        },
        showStockAvailable: function () {
            this.isLoading(true);
            var self = this;
            $.ajax({
                url: self.url,
                data: {
                    "sku": self.sku,
                    "id": self.id
                },
                type: 'post',
                dataType: 'json'})
                .always(function () {
                    self.isLoaded(false);
                })
                .done(function (data) {
                    console.log(data);
                    if (data.productCount) {
                        self.productCount(data.productCount);
                        self.isLoaded(true)
                    }
                })
                .always(function () {
                self.isLoading(false);
            });
        }
    });
});