<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-28
 * Time: 22:31
 */

namespace Training\ProductCount\Controller\AvailableStock;


use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonResultFactory;

    /**
     * @var \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface
     */
    private $stockInterface;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface $stockStateInterface
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        Context $context,
        GetProductSalableQtyInterface $stockStateInterface,
        JsonFactory $jsonResultFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->stockInterface = $stockStateInterface;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->request = $request;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $jsonResult = $this->jsonResultFactory->create();
        return $jsonResult->setData(["productCount" => $this->stockInterface->execute($this->request->getParam('sku'), $this->request->getParam('id'))]);
    }
}
