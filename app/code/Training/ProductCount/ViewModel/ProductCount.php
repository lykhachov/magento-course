<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-21
 * Time: 12:36
 */

namespace Training\ProductCount\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class ProductCount implements ArgumentInterface
{
    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $product;

    /**
     * ProductCount constructor.
     * @param \Magento\Catalog\Block\Product\View $product
     */
    public function __construct(
        \Magento\Catalog\Block\Product\View $product
    ) {
        $this->product = $product->getProduct();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->product->getStoreId();
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->product->getSku();
    }

}
