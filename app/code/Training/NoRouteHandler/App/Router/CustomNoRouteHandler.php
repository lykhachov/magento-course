<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-12
 * Time: 16:42
 */

namespace Training\NoRouteHandler\App\Router;

use Magento\Framework\App\Router\NoRouteHandlerInterface;

class CustomNoRouteHandler implements NoRouteHandlerInterface
{
    public function process(\Magento\Framework\App\RequestInterface $request)
    {
        $moduleName = 'cms';
        $controllerPath = 'index';
        $controllerName = 'index';
        $request->setModuleName($moduleName)->setControllerName($controllerPath)->setActionName($controllerName);
        return true;
    }
}
