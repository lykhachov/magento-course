<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-15
 * Time: 21:30
 */

namespace Training\FeedbackProduct\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class LoadFeedbackProducts implements ObserverInterface
{
    private $feedbackProducts;

    /**
     * SaveFeedbackProducts constructor.
     */
    public function __construct(
        \Training\FeedbackProduct\Model\FeedbackProducts $feedbackProducts
    ) {
        $this->feedbackProducts = $feedbackProducts;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $feedback = $observer->getData('feedback');
        $this->feedbackProducts->loadProductRelations($feedback);
    }

}
