<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-15
 * Time: 17:25
 */

namespace Training\FeedbackProduct\Plugin\Api\Data;

use Training\Feedback\Api\Data\FeedbackExtensionInterfaceFactory;

class FeedbackInterfaceExtension
{
    /**
     * @var FeedbackExtensionInterfaceFactory $extensionAttributesFactory
     */
    private $extensionAttributesFactory;

    public function __construct(
        FeedbackExtensionInterfaceFactory $extensionAttributesFactory
    ) {
        $this->extensionAttributesFactory = $extensionAttributesFactory;
    }

    /**
     * @param \Training\Feedback\Api\Data\FeedbackInterface $subject
     * @param $result
     * @return \Training\Feedback\Api\Data\FeedbackExtensionInterface
     */
    public function afterGetExtensionAttributes(
        \Training\Feedback\Api\Data\FeedbackInterface $subject,
        $result
    ) {
        if (!is_null($result)) {
            return $result;
        }
        /** @var \Training\Feedback\Api\Data\FeedbackExtensionInterface $extensionAttributes */
        $extensionAttributes = $this->extensionAttributesFactory->create();
        $subject->setExtensionAttributes($extensionAttributes);
        return $extensionAttributes;
    }
}