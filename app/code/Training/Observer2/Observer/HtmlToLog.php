<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-11
 * Time: 11:21
 */

namespace Training\Observer2\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class HtmlToLog implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        $response = $observer->getEvent()->getData('response');
        $this->logger->debug($response);
    }
}
