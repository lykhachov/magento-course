<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-19
 * Time: 09:36
 */

namespace Training\Rendering\Block;

class Test extends \Magento\Framework\View\Element\AbstractBlock
{
    protected function _toHtml()
    {
        return '<b>test block</b>';
    }
}
