var config = {
    config: {
        mixins: {
            'Magento_Catalog/js/catalog-add-to-cart': {
                'Training_Rendering/js/catalog-add-to-cart/mixin': true
            },
            'Magento_checkout/js/action/place-order': {
                'Training_Rendering/js/action/place-order/mixin': true
            }
        }
    }
};
