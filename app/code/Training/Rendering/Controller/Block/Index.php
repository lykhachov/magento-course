<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-19
 * Time: 09:57
 */

namespace Training\Rendering\Controller\Block;

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    private $layoutFactory;
    private $resultRawFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->resultRawFactory = $resultRawFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $layout = $this->layoutFactory->create();
        $block = $layout->createBlock(\Training\Rendering\Block\Test::class);
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents('Raw: ' . $block->toHtml());
    }
}
