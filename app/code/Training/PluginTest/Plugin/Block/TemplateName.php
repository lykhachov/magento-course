<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-07
 * Time: 12:31
 */

namespace Training\PluginTest\Plugin\Block;

class TemplateName
{
    public function afterToHtml(
        \Magento\Framework\View\Element\Template $subject,
        $result
    ) {
        if ($subject->getNameInLayout() == 'top.search') {
            $result = '<div><p>' . $subject->getTemplate() . '</p>' . '<p>' . get_class($subject) . '</p>' . $result . '</div>';
        }
        return $result;
    }
}