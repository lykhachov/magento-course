<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-07
 * Time: 14:53
 */

namespace Training\PluginTest\Plugin\Controller;

class Redirect
{
    protected $customerSession;
    protected $redirectFactory;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
    ) {
        $this->customerSession = $customerSession;
        $this->redirectFactory = $redirectFactory;
    }

    public function aroundExecute(
        \Magento\Catalog\Controller\Product\View $subject,
        \Closure $proceed
    ) {
        echo 'test';
        if (!$this->customerSession->isLoggedIn()) {
            return $this->redirectFactory->create()->setPath('customer/account/login');
        }
        return $proceed();
    }
}