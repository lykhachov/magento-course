<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-06
 * Time: 20:55
 */

namespace Training\PluginTest\Plugin\Model;

class Url
{
    public function beforeGetUrl(
        \Magento\Framework\UrlInterface $subject,
        $routePath = null,
        $routeParams = null
    ) {
        if ($routePath == 'customer/account/create') {
            return ['customer/account/login', null];
        }
    }
}
