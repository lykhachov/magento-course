<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-13
 * Time: 21:44
 */

namespace Training\Feedback\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Training\Feedback\Api\Data\FeedbackInterface;

class Test extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Training\Feedback\Api\Data\FeedbackInterfaceFactory $feedbackFactory
     */
    private $feedbackFactory;
    /**
     * @var \Training\Feedback\Api\Data\FeedbackRepositoryInterface $feedbackRepository
     */
    private $feedbackRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder
     */
    private $sortOrderBuilder;

    public function __construct(
        Context $context,
        \Training\Feedback\Api\Data\FeedbackInterfaceFactory $feedbackFactory,
        \Training\Feedback\Api\Data\FeedbackRepositoryInterface $feedbackRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder
    ) {
        $this->feedbackFactory = $feedbackFactory;
        $this->feedbackRepository = $feedbackRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        //create new item
        $newFeedback = $this->feedbackFactory->create();
        $newFeedback
            ->setAuthorName('test name')
            ->setAuthorEmail('test@test.com')
            ->setMessage('this is the test message!!!')
            ->setIsActive(1);
        $this->feedbackRepository->save($newFeedback);

        //load item by id
        $feedback = $this->feedbackRepository->getById(2);
        $this->printFeedback($feedback);

        //update item
        $feedbackToUpdate = $this->feedbackRepository->getById(2);
        $feedbackToUpdate->setMessage('CUSTOM ' . $feedbackToUpdate->getMessage());
        $this->feedbackRepository->save($feedbackToUpdate);

        //delete feedback
        $this->feedbackRepository->deleteById(1);

        //load multiple items
        $sampleFeedback = $this->feedbackFactory->create();
        $this->searchCriteriaBuilder
            ->addFilter($sampleFeedback::IS_ACTIVE, 1);
        $sortOrder = $this->sortOrderBuilder
            ->setField($sampleFeedback::CREATION_TIME)
            ->setAscendingDirection()
            ->create();
        $this->searchCriteriaBuilder->addSortOrder($sortOrder);

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $searchResult = $this->feedbackRepository->getList($searchCriteria);
        foreach ($searchResult->getItems() as $item) {
            $this->printFeedback($item);
        }
        exit();
    }

    private function printFeedback(FeedbackInterface $feedback)
    {
        echo $feedback->getId() . ' : '
            . $feedback->getAuthorName()
            . ' (' . $feedback->getAuthorEmail() . ') '
            . ' message: ' . $feedback->getMessage();
        echo "<br/>\n";
    }
}
