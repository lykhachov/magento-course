<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-07
 * Time: 22:06
 */

namespace Training\Feedback\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    private $pageResultFactory;

    /**
     * @param Context $context
     * @param PageFactory $pageResultFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageResultFactory
    ) {
        $this->pageResultFactory = $pageResultFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->pageResultFactory->create();
        return $result;
    }
}
