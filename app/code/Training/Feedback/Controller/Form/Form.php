<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-07
 * Time: 15:47
 */

namespace Training\Feedback\Controller\Form;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Form extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory PageFactory
     */
    private $pageResultFactory;

    /**
     * @param Context $context
     * @param PageFactory $pageResultFactory
     */
    public function __construct(Context $context, PageFactory $pageResultFactory)
    {
        $this->pageResultFactory = $pageResultFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $result = $this->pageResultFactory->create();
        return $result;
    }
}
