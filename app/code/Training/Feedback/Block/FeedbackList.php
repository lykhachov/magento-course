<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-07
 * Time: 22:16
 */

namespace Training\Feedback\Block;

use Magento\Framework\Stdlib\DateTime\Timezone;
use Magento\Framework\View\Element\Template;

class FeedbackList extends Template
{
    const PAGE_SIZE = 5;

    /**
     * @var \Training\Feedback\Model\ResourceModel\Feedback\CollectionFactory $collectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Training\Feedback\Model\ResourceModel\Feedback\Collection $collection
     */
    private $collection;
    /**
     * @var Magento\Framework\Stdlib\DateTime\Timezone $timezone
     */
    private $timezone;
    /**
     * @var \Training\Feedback\Model\ResourceModel\Feedback $feedbackResource
     */
    private $feedbackResource;

    /**
     * FeedbackList constructor.
     * @param Template\Context $context
     * @param \Training\Feedback\Model\ResourceModel\Feedback\CollectionFactory $collectionFactory
     * @param \Training\Feedback\Model\ResourceModel\Feedback $feedbackResource
     * @param Timezone $timezone
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Training\Feedback\Model\ResourceModel\Feedback\CollectionFactory $collectionFactory,
        \Training\Feedback\Model\ResourceModel\Feedback $feedbackResource,
        Timezone $timezone,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->feedbackResource = $feedbackResource;
        $this->timezone = $timezone;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getFeedbackCollection()
    {
        if (!$this->collection) {
            $this->collection = $this->collectionFactory->create();
            $this->collection->addFieldToFilter('is_active', 1);
            $this->collection->setOrder('creation_time', 'DESC');
        }
        return $this->collection;
    }

    /**
     * @return string
     */
    public function getPaginationHtml()
    {
        $pagerBlock = $this->getChildBlock('feedback_list_pager');

        if ($pagerBlock instanceof \Magento\Framework\DataObject) {
            /* @var $pagerBlock \Magento\Theme\Block\Html\Pager */
            $pagerBlock
                ->setUseContainer(false)
                ->setShowPerPage(false)
                ->setShowAmounts(false)
                ->setLimit($this->getLimit())
                ->setCollection($this->getFeedbackCollection());
            return $pagerBlock->toHtml();
        }
        return 'n/a';
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return static::PAGE_SIZE;
    }

    /**
     * @return string
     */
    public function getAddFeedbackUrl()
    {
        return $this->getUrl('training_feedback/form/form');
    }

    /**
     * @param $feedback
     * @return string
     */
    public function getFeedbackDate($feedback)
    {
        return $this->timezone->formatDateTime($feedback->getData('update_time'));
    }

    /**
     * @return string
     */
    public function getAllFeedbackNumber()
    {
        return $this->feedbackResource->getAllFeedbackNumber();
    }

    /**
     * @return string
     */
    public function getActiveFeedbackNumber()
    {
        return $this->feedbackResource->getActiveFeedbackNumber();
    }
}
