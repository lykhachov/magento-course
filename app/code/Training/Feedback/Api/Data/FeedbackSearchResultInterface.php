<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-03-13
 * Time: 20:37
 */

namespace Training\Feedback\Api\Data;


use Magento\Framework\Api\SearchResultsInterface;

interface FeedbackSearchResultInterface extends SearchResultsInterface
{
    /**
     * Get Feedback list.
     *
     * @return \Training\Feedback\Api\Data\FeedbackInterface[]
     */
    public function getItems();

    /**
     * Set Feedback list.
     *
     * @param \Training\Feedback\Api\Data\FeedbackInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
