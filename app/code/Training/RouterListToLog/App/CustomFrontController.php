<?php
/**
 * Created by PhpStorm.
 * User: lykhachov
 * Date: 2019-02-11
 * Time: 14:46
 */

namespace Training\RouterListToLog\App;

use Magento\Framework\App\FrontController;
use Magento\Framework\App\Request\ValidatorInterface as RequestValidator;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterListInterface;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Psr\Log\LoggerInterface;

class CustomFrontController extends FrontController
{
    /**
     * @var RouterListInterface
     */
    protected $routerList;
    /**
     * @var ResponseInterface
     */
    protected $response;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        RouterListInterface $routerList,
        ResponseInterface $response,
        RequestValidator $requestValidator,
        MessageManager $messageManager,
        LoggerInterface $logger
    ) {
        $this->routerList = $routerList;
        $this->logger = $logger;
        $this->response = $response;
        parent::__construct($routerList, $response, $requestValidator, $messageManager, $logger);
    }

    public function dispatch(RequestInterface $request)
    {
        foreach ($this->routerList as $router) :
            $this->logger->info(get_class($router));
        endforeach;
        return parent::dispatch($request);
    }
}
